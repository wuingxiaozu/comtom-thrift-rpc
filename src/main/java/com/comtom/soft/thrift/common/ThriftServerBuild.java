package com.comtom.soft.thrift.common;

import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author nobody
 * thriftServer 构建工具
 */
public class ThriftServerBuild {
	
    private final static Logger logger = LoggerFactory.getLogger(ThriftServerBuild.class);

	private ThriftServerBuild() {
		super();
	}
    
    public static TServer buildServer(Integer port,TProcessor processor){
    	TServer server=null;
		try {
			TNonblockingServerSocket serverTransport = new TNonblockingServerSocket(port);
			TThreadedSelectorServer.Args tArgs = new TThreadedSelectorServer.Args(serverTransport);  
			tArgs.processor(processor);
			tArgs.protocolFactory( new TCompactProtocol.Factory()); 
			tArgs.transportFactory(new TFramedTransport.Factory());  
			server=new TThreadedSelectorServer(tArgs); 
		} catch (Exception e) {
			logger.error("构建ThriftServer失败:"+e.getMessage());
			e.printStackTrace();
		}
		return server;
    }
}
