package com.comtom.soft.thrift.server;

import org.apache.thrift.TProcessor;
import org.apache.thrift.server.TServer;

import com.comtom.soft.thrift.common.IpNetworkResolve;
import com.comtom.soft.thrift.common.RegisterUtil;
import com.comtom.soft.thrift.common.ThriftServerBuild;
import com.comtom.soft.thrift.service.IpResolve;
import com.comtom.soft.thrift.service.ThriftServerRegister;

/**
 * @author nobody
 * 单服务对应的thrift 服务器
 */
public class SingleThriftServerFactory extends ThriftServerFactory{

	// 服务实现类
	private Object service;// serice实现类

	private IpResolve ipResolve;
	
	private String version="";
	
	private ThriftServerRegister thriftServerRegister;
	
	@Override
	public TServer buildServer() {
		Class<?> clazz=RegisterUtil.getIfaceClass(service);
		TProcessor tprocessor = RegisterUtil.constructorProssor(clazz, service);
		
		TServer tserver= ThriftServerBuild.buildServer(port, tprocessor);
		
		if(ipResolve==null){
			ipResolve=new IpNetworkResolve();
		}
		String serviceClass=clazz.getEnclosingClass().getName();
		String addressBuild=ipResolve.getServerIp()+":"+port;
		thriftServerRegister.register(serviceClass, version, addressBuild);
		return tserver;
	}

	/**
	 * @return the service
	 */
	public Object getService() {
		return service;
	}

	/**
	 * @param service the service to set
	 */
	public void setService(Object service) {
		this.service = service;
	}

	/**
	 * @return the ipResolve
	 */
	public IpResolve getIpResolve() {
		return ipResolve;
	}

	/**
	 * @param ipResolve the ipResolve to set
	 */
	public void setIpResolve(IpResolve ipResolve) {
		this.ipResolve = ipResolve;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the thriftServerRegister
	 */
	public ThriftServerRegister getThriftServerRegister() {
		return thriftServerRegister;
	}

	/**
	 * @param thriftServerRegister the thriftServerRegister to set
	 */
	public void setThriftServerRegister(ThriftServerRegister thriftServerRegister) {
		this.thriftServerRegister = thriftServerRegister;
	}
	
	
}
