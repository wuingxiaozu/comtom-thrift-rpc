package com.comtom.soft.thrift.server;

import org.apache.thrift.TMultiplexedProcessor;
import org.apache.thrift.server.TServer;

import com.comtom.soft.thrift.common.ThriftServerBuild;

/**
 * @author nobody
 * 多服务对于的服务器
 */
public class MultyThriftServerFactory extends ThriftServerFactory{

	//ThriftServer的处理器
	private TMultiplexedProcessor processor;

	@Override
	public TServer buildServer() {
		 return ThriftServerBuild.buildServer(port, processor);
	}
	
	/**
	 * @param processor the processor to set
	 */
	public void setProcessor(TMultiplexedProcessor processor) {
		this.processor = processor;
	}
}
