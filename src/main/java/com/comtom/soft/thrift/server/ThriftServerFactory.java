package com.comtom.soft.thrift.server;

import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.thrift.server.TServer;

/**
 * 服务端工厂
 */
public abstract class ThriftServerFactory {
	// 服务注册本机端口
	protected Integer port = 8299;
	//ThriftServer 对象
	private TServer server;

	public void setPort(Integer port) {
		this.port = port;
	}

	@PostConstruct
	public void init(){
		server=buildServer();
		Executors.newSingleThreadExecutor().execute(new Runnable() {
			@Override
			public void run() {
				server.serve();
			}
		});
	}
	
	@PreDestroy
	public void destory(){
		server.stop();
		server=null;
	}

	public abstract TServer buildServer();
	
}
