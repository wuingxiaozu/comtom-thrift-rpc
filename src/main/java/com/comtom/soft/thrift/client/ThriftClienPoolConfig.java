package com.comtom.soft.thrift.client;

public class ThriftClienPoolConfig {
	//最多活跃的连接数
	private Integer maxActive;
	
	//链接等待时间,超时则异常
	private Integer maxWait;
	
	//最大的空闲链接
	private Integer maxIdle;
	
	//最小的空闲链接
	private Integer minIdle;

	/**
	 * @return the maxActive
	 */
	public Integer getMaxActive() {
		return maxActive;
	}

	/**
	 * @param maxActive the maxActive to set
	 */
	public void setMaxActive(Integer maxActive) {
		this.maxActive = maxActive;
	}

	/**
	 * @return the maxWait
	 */
	public Integer getMaxWait() {
		return maxWait;
	}

	/**
	 * @param maxWait the maxWait to set
	 */
	public void setMaxWait(Integer maxWait) {
		this.maxWait = maxWait;
	}

	/**
	 * @return the maxIdle
	 */
	public Integer getMaxIdle() {
		return maxIdle;
	}

	/**
	 * @param maxIdle the maxIdle to set
	 */
	public void setMaxIdle(Integer maxIdle) {
		this.maxIdle = maxIdle;
	}

	/**
	 * @return the minIdle
	 */
	public Integer getMinIdle() {
		return minIdle;
	}

	/**
	 * @param minIdle the minIdle to set
	 */
	public void setMinIdle(Integer minIdle) {
		this.minIdle = minIdle;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ThriftClienPoolConfig [maxActive=" + maxActive + ", maxWait="
				+ maxWait + ", maxIdle=" + maxIdle + ", minIdle=" + minIdle
				+ "]";
	}
	
}
