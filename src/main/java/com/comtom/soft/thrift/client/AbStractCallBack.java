package com.comtom.soft.thrift.client;

import org.apache.thrift.async.AsyncMethodCallback;

/**
 * 异步调用方法保证，同一个客户端同一时刻只能调用一个方法
 * @author nobody
 *
 * @param <T>
 */
public class AbStractCallBack<T> implements AsyncMethodCallback<T>{

	private AsyncMethodCallback<T> callBack;
	
	private AsyncClientAdapter adapter;
	
	@Override
	public void onComplete(T response) {
		try {
			callBack.onComplete(response);
		} finally{
			adapter.setCallIsRuning(false);
		}
	}

	@Override
	public void onError(Exception exception) {
		try {
			callBack.onError(exception);
		} finally{
			adapter.setCallIsRuning(false);
		}
	}

	public AbStractCallBack(AsyncMethodCallback<T> callBack,AsyncClientAdapter adapter) {
		super();
		this.callBack = callBack;
		this.adapter = adapter;
	}


}
