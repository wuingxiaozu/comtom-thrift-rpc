package com.comtom.soft.thrift.client;

import java.net.InetSocketAddress;

public class ServiceSocketAddress extends InetSocketAddress {

	private static final long serialVersionUID = 1L;
	
	//server公布的服务名称
	private String serviceName;
	
	public ServiceSocketAddress(String addr, int port, String serviceName) {
		super(addr, port);
		this.serviceName=serviceName;
	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

}
