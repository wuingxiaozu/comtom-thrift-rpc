package com.comtom.soft.thrift.client;

import org.apache.thrift.async.TAsyncClient;
import org.apache.thrift.transport.TNonblockingTransport;

/**
 * @author nobody
 * 异步客户端适配器
 */
public class AsyncClientAdapter {

	private TNonblockingTransport transport;

	private TAsyncClient asyncClient;

	/**
	 * 是否正在调用
	 */
	private boolean callIsRuning=false;

	
	/**
	 * @return the asyncClient
	 */
	public TAsyncClient getAsyncClient() {
		return asyncClient;
	}


	/**
	 * @param asyncClient the asyncClient to set
	 */
	public void setAsyncClient(TAsyncClient asyncClient) {
		this.asyncClient = asyncClient;
	}



	/**
	 * @return the callIsRuning
	 */
	public boolean isCallIsRuning() {
		return callIsRuning;
	}



	/**
	 * @param callIsRuning the callIsRuning to set
	 */
	public void setCallIsRuning(boolean callIsRuning) {
		this.callIsRuning = callIsRuning;
	}



	/**
	 * @param transport the transport to set
	 */
	public void setTransport(TNonblockingTransport transport) {
		this.transport = transport;
	}



	/**
	 * @return the transport
	 */
	public TNonblockingTransport getTransport() {
		return transport;
	}

}
