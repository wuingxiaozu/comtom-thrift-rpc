package cn.slimsmart.thrift.rpc.demo;

import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.comtom.soft.thrift.client.AbStractCallBack;

import cn.slimsmart.thrift.rpc.demo.service.EchoSerivce;
import cn.slimsmart.thrift.rpc.demo.service.EchoSerivce.AsyncClient.echo_call;
import cn.slimsmart.thrift.rpc.demo.service.EchoSerivce.AsyncIface;

//客户端调用
@SuppressWarnings("resource")
public class Client {
	public static void main(String[] args) {
//		simple();
		spring();
		springAsync();
	}

	public static void spring() {
		try {
			ApplicationContext context = new ClassPathXmlApplicationContext("spring-context-thrift-client.xml");
			Thread.sleep(1000);
//			System.in.read();
			EchoSerivce.Iface echoSerivce = (EchoSerivce.Iface) context.getBean("echoSerivce");
//			HelloWorldService.Iface helloService = (HelloWorldService.Iface) context.getBean("helloService");
//			Thread.sleep(1000);
//			for (int i = 0; i < 100; i++) {
//				Thread.sleep(3000);
//				try {
					System.err.println(echoSerivce.echo("hello----"));
//					System.out.println(helloService.sayHello("DDDDD"));
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				//TThread t = new TThread(echoSerivce);
//				//t.start();
//			}
//			Thread.sleep(3000000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static <T> void springAsync() {
		try {
			ApplicationContext context = new ClassPathXmlApplicationContext("spring-context-thrift-async-client.xml");
			Thread.sleep(1000);
			EchoSerivce.AsyncIface iface=(AsyncIface) context.getBean("asyncEchoSerivce");
			AsyncMethodCallback<echo_call> callback=new AsyncMethodCallback<echo_call>() {
				@Override
				public void onComplete(echo_call response) {
					try {
						System.err.println(response.getResult().toString());
					} catch (TException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onError(Exception exception) {
					System.out.println(exception);
				}
			};
			iface.echo("DDDD", callback);
			iface.echo("DDDD", callback);
			Thread.sleep(3000000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static class TThread extends Thread {
		EchoSerivce.Iface echoSerivce;

		TThread(EchoSerivce.Iface service) {
			echoSerivce = service;
		}

		public void run() {
			try {
				for (int i = 0; i < 10; i++) {
					Thread.sleep(1000*i);
					System.out.println(Thread.currentThread().getName()+"  "+echoSerivce.echo("hello"));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void simple() {
		try {
			TSocket socket = new TSocket("localhost", 10000);
			TTransport transport = new TFramedTransport(socket);
			TProtocol protocol = new TBinaryProtocol(transport);
			EchoSerivce.Client client = new EchoSerivce.Client(protocol);
			transport.open();
			System.out.println(client.echo("helloword"));
			Thread.sleep(3000);
			transport.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
